'use strict'

var Valine = require('./dist/Valine.min.js')
var vueComment =  require('./other-valine/vue/components/vue-comment/comment').default;
module.exports = {
    Valine,
    vueComment
}
module.exports.default = module.exports