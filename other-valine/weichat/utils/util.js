
var utils = {};
Date.prototype.Format = function (fmt) { //author: meizz 
  var o = {
    "M+": this.getMonth() + 1, //月份 
    "d+": this.getDate(), //日 
    "h+": this.getHours(), //小时 
    "m+": this.getMinutes(), //分 
    "s+": this.getSeconds(), //秒 
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
    "S": this.getMilliseconds() //毫秒 
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

//



//数据提交时间
utils.getMsgTime = function (num) {
  var currT = new Date();
  var createT = new Date(num);
  var currNum = currT.getTime();
  var creatNum = createT.getTime();
  var miss = currNum - creatNum;
  miss = miss / 1000 / 60; //以分为单位
  if (miss <= 1) {
    return "刚刚";
  }

  if (miss < 60) {
    return Math.floor(miss) + "分钟前";
  }

  miss = miss / 60; //以小时为单位
  if (miss < 24) {
    return Math.floor(miss) + "小时前";
  }

  miss = miss / 24; //以天为单位
  if (miss <= 3) {
    return Math.floor(miss) + "天前";
  }
  var currY = currT.getFullYear();
  var cY = createT.getFullYear();
  if (currY == cY) {
    return createT.Format("MM-dd");
  }

  if (currY > cY) {
    return createT.Format("yyyy-MM-dd");
  }
  return "刚刚";
}

//动态时间转化 n days 
utils.getDateByReg = function (val) {
  var curr = new Date();
  var date = "";
  if (val) {
    var reg = /today/ig;
    if (!reg.test(val)) {
      var num = parseFloat(val);
      console.log(num);
      if (num || num === 0) {
        if ((/d/ig).test(val)) {
          curr.setDate(curr.getDate() + num);
        }
        if ((/m/ig).test(val)) {
          curr.setMonth(curr.getMonth() + num);
        }
        if ((/w/ig).test(val)) {
          curr.setDate(curr.getDate() + num * 7);
        }
      }
    }
    date = curr.Format("yyyy-MM-dd");
  }
  if (curr == "Invalid Date") {
    date = "";
  }
  console.log(val, date);
  return date;
};


//格式化文件大小
utils.renderSize = function (value) {
  if (null == value || value == '') {
    return "0KB";
  }
  if (value < 0) {
    return "0kB"
  }
  var unitArr = new Array("KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
  var index = 0;
  var srcsize = parseFloat(value);
  index = Math.floor(Math.log(srcsize) / Math.log(1024));
  var size = srcsize / Math.pow(1024, index);
  size = size.toFixed(2);//保留的小数位数
  return size + unitArr[index];
};




//提示
utils.alertShow = function (title, time) {
  time = time || 1200;
  title = title || "服务器或者网络错误";
  setTimeout(function () {
    wx.showToast({
      title: title,
      icon: 'none',
      duration: time
    })
  }, 0);

}

//后去页面对象栈
utils.getPage = function (index) {
  index = index || 1;
  if (index <= 0) {
    index = 1;
  }

  var pages = getCurrentPages();
  var prevPage = pages[pages.length - index];  //1为当前页面，2为上一个页面 
  return prevPage;
};

//生成名字
utils.getImgName = function () {
  var time = new Date().getTime();
  var rand = Math.floor(Math.random() * 9000000000 + 1000000000);
  return time + '' + rand;
};



//校验数据
utils.check = {};

//手机号码
utils.check.mobile = function (data, hide) {
  var reg = /^(13|14|15|17|18|19)[0-9]{9}$/;
  if (!reg.test(data)) {
    if (!hide) {
      utils.alertShow("请填写正确的手机号");
    }
    return false;
  }
  return true;
};

//Email
utils.check.email = function (data, hide) {
  var reg = /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/;
  if (!reg.test(data)) {
    if (!hide) {
      utils.alertShow("请填写正确格式的Email地址");
    }

    return false;
  }
  return true;
};

//qq
utils.check.qq = function (data) {
  var reg = /^[1-9]([0-9]{5,11})$/;
  if (!reg.test(data)) {
    utils.alertShow("请填写正确的QQ号");
    return false;
  }
  return true;
};


//getyp

function setTp(typ) {
  return function (obj) {
    return Object.prototype.toString.call(obj) === "[object " + typ + "]"
  }
}
utils.isArray = setTp("Array");
utils.isFunction = setTp("Function");
utils.isObject = setTp("Object");
utils.isString = setTp("String");
utils.isNumber = setTp("Number");

var blockElement = ['div', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
//去掉html
utils.trimHtml = function (str, trimBr) {

  if (utils.isString(str)) {
    if (!trimBr) {
      //保留br,将br替换成 \n
      str = str.replace(/<[br][br \/]>/ig, "\n");
      //保留快级元素换行
      for (var i = 0; i < blockElement.length; i++) {
        //处理html里面的块级元素换行
        var item = blockElement[i];
        var start = "<" + item + ">";
        var end = "</" + item + ">";
        var regs = new RegExp(start, 'ig');
        var regd = new RegExp(end, 'ig');
        str = str.replace(regs, '\n');
        str = str.replace(regd, '\n');
      }

      str = str.replace(/\n\n/ig, '\n');
    }
    return str.replace(/<[^>]+>/g, "");
  } else {
    return '';
  }
}

module.exports = utils;
