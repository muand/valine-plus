# valine-plus

#### 介绍
使用开源的库 [valine](https://valine.js.org/configuration.html#verify) 的代码，进行二次修改，能够自定义头像，昵称等。
具体使用，参考 index.html 的例子，修改后的参数，与原来文档参数，有一定的区别
#### 软件架构


#### 安装教程

1. 开发和使用  npm i & npm run dev
2. 打包：  npm run build
3. 通过 npm run build 打包后，在 dist /Valine.min.js 文件，可以复制出来，以供使用。 也可以发布到 npm 私有仓库后使用
#### 使用说明

1. 该项目来源于valine 开源项目进行的修改，其中提供了自定义的头像、昵称、加入提交的钩子函数。方便外部控制
2. 其在vue 的使用方式，与原始文档一致，只是参数有稍微调整。
3. vue 中使用时，请使用  npm install valine-plus --save
4. 安装后请在 package 中设置版本号为：  "valine-plus": "latest"
5. 使用 require 引入后， 初始化实例 ， valine.VERSION 查看版本  
#### 参与贡献

1. 源码地址 git clone https://gitee.com/muand/valine-plus.git 


#### 码云特技

1. [参考来源](https://valine.js.org/configuration.html#verify)
2. [参考git地址](https://github.com/xCss/Valine)
