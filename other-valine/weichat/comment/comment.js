const AV = require('../../utils/av-weapp-min.js');
const utilComm = require("./util-comm.js");
const utils = require("../../utils/util.js");
// LeanCloud 应用的 ID 和 Key


Component({
  properties: {
    person:{  //评论的人附加字段
      type:Object,
      value:{
        nick: '游客',
        attach: {},
        ua: 'wxapp',
        avatar: "",
        url: "" //标识 id,针对哪个的标识
      }
    },
    config:{
      type: Object,
      // value:{
      //   path:"",      //很重要的标识
      //   defAvatar: "", //有些评论没有头像的，给个默认头像
      //   defNick: "", //有些评论如果没有名字，就使用默认的
      //   appId: "",
      //   appKey: ""
      // },
      observer: function (newVal, oldVal) {
        // 属性值变化时执行
        if (newVal) {
          const appId = newVal.appId;
          const appKey = newVal.appKey;
          const path = newVal.path;
          if (appId && appKey && path){
            AV.init({
              appId: appId,
              appKey: appKey,
              serverURLs: utilComm.getServerURLs(AV, appId)  //通过appid获取
            });
            this.query();
            this.initCount();
          }
         
        }
      }
    },
    table:{  //使用的表
      type:String,
      value:"Comment"
    },
    pageSize:{  //分页
      type:Number,
      value:10
    }
  },
  data: {
    list:[],
    atData:"",
    comment:"",
    placeholder:"请输入评论",
    haseData:true,
    count:'',
    currtPage:1,
    cacheId:{},  //缓存所有的评论 id 防止新的数据分页重复
    hideComment:true,
    autoFocus:false
  },
  created(){
    //console.log(this.properties.person.url);

  },
  /**
  * 页面上拉触底事件的处理函数
  */
  methods: {
    commInput(e){
      var value = e.detail.value;
      value = value.trim();
      this.setData({
        comment:value
      })
    },
    Q(k,ids){
      const me = this;
      const table = me.properties.table;
      if (!ids){
        let notExist = new AV.Query(table);
        notExist.doesNotExist('rid');
        let isEmpty = new AV.Query(table);
        isEmpty.equalTo('rid', '');
        let q = AV.Query.or(notExist, isEmpty);
        if (k === '*') q.exists('url');
        else q.equalTo('url', decodeURI(k));
        q.addDescending('createdAt');
        q.addDescending('insertedAt');
        return q;
      }else{
        ids = JSON.stringify(ids).replace(/(\[|\])/g, '');
        let cql = `select * from ${table} where rid in (${ids}) order by createdAt,updatedAt asc `;
        return AV.Query.doCloudQuery(cql);
      }
    },
    async query(){
      const me = this;

      if(!me.data.haseData){
        return ;
      }

      let no = me.data.currtPage;

      const url = me.properties.config.path;
      const size = me.properties.pageSize;

      var list = me.data.list;
      let cacheId = me.data.cacheId; //防止数据分页重复
      try{
        wx.showLoading({
          title: '加载中',
          mask: true
        });
        let cq = me.Q(url);
        cq.limit(size);
        cq.skip((no - 1) * size);
        var rets = await cq.find();
        let len = rets.length;
        let retsData = [];
        let cache = {};
        
        let rids = [];
        for (let i = 0; i < len; i++) {
          let ret = utilComm.formatItem(rets[i]);
          if (!cacheId[ret.id]){
            rids.push(ret.id);
            retsData.push(ret);
            cache[ret.id] = ret;
            cacheId[ret.id] = true;
          }
        }
        //加载子节点
        if (rids.length > 0) {
          let retChilds = await me.Q(url, rids);
          let childs = retChilds && retChilds.results || [];
          for (let k = 0; k < childs.length; k++) {
            let child = utilComm.formatItem(childs[k]);
            let parent = cache[child["rid"]];
            if (parent){
              if(!parent._childs){
                parent._childs = [];
              }
              parent._childs.push(child);
            }
          }
        }

        if (len < size){
          me.setData({
            haseData:false
          })
        }

       

        me.setData({
          list: list.concat(retsData),
          currtPage: ++no,
          cacheId: cacheId
        });
        
      }catch(e){
        console.log(e);
      }

      wx.hideLoading();
    },
    async initCount(){
      var count = await this.getCount();
      this.setData({
        count
      })
    },
    async getCount(){
      //获取评论的总数
      try{
        if (this.data.count === "") {
          //说明还没有请求过
          const url = this.properties.config.path;
          let count = this.Q(url).count();
          return Promise.resolve(count);
        } else {
          return Promise.resolve(this.data.count);
        }
      }catch(e){
        console.log(e);
        return Promise.resolve(0);
      }
      
    },
  
    async commitEvt(commentTxt){
      //提交
      try{
        wx.showLoading({
          title: '正在提交',
          mask: true
        });

        const me = this;
        const table = me.properties.table;
        const url = me.properties.config.path;
        let atData = me.data.atData;
        let Ct = AV.Object.extend(table);
        // 新建对象
        let comment = new Ct();

        let person = me.properties.person
        for (var i in person) {
          comment.set(i, person[i]);
        }

        comment.set('url', decodeURI(url));
        comment.set('insertedAt', new Date());
        if (atData) {
          let pid = atData['pid'] || atData['rid'];
          comment.set('rid', atData['rid']);
          comment.set('pid', pid);
          comment.set('at', atData['at']);    //艾特的人的用户名
          comment.set('atattach', atData['atattach']); //被艾特的人的附加数据
        }
        comment.set("comment", commentTxt);
        comment.setACL(me.getAcl()); //设置权限，可以读写
        let ret = await comment.save();
        
        let data = utilComm.formatItem(ret);

        let list = me.data.list;
        if (atData) {
          //插入子节点
          for (let i = 0; i < list.length; i++) {
            let item = list[i];
            if (item.id == data.rid) {
              if (!item._childs){
                item._childs = [];
              }
              item._childs.push(data);
              break;
            }
          }
        }else{
          //评论数加一
          let count = await this.getCount();
          count++;
          //追加节点
          list.unshift(data);
          me.setData({
            count,
            [`cacheId.${data.id}`]:true
          })
        }

        me.setData({
          list
        });

        me.reset();

      }catch(e){
        console.log(e)
      }
      wx.hideLoading();
    },
   
    getAcl(){
      let acl = new AV.ACL();
      acl.setPublicReadAccess(true);
      acl.setPublicWriteAccess(false);
      return acl;
    },
    reset(){
      this.setData({
        comment:"",
        atData:"",
        placeholder:"请输入评论",
        hideComment: true,
        autoFocus:false
      })
    },
    /*事件*/
    bindFormSubmit(e) {
      //点击评论
      var comment = e.detail.value.comment;
      console.log(comment);
      comment = comment.trim();
      if (!comment) {
        //utils.alertShow("内容不能为空");
        return;
      }
      this.commitEvt(comment);
    },
    atReply(e){
      //艾特 回复
      var item = e.currentTarget.dataset.item;
      atData = {
        'at': item.nick,
        'atattach': item.attach || {}, //被at的附加信息
        'rid': item.rid || item.id, //  rt.id 自己的id ， rt.get("rid") 根评论的id， rt.get("pid") 父级评论的id
        'pid': item.id,
        'rt': item
      }

      this.setData({
        atData,
        placeholder: '回复：' + item.nick + ' '
      });
      this.showComment();
    },
    showComment(){
      this.setData({
        hideComment: false
      },function(){
        this.setData({
          autoFocus:true
        })
      });
    },
    cancel(){
      this.reset();
    },
    clickWrap(e){},
    clickName(e){
      //点击用户的名字
      var item = e.currentTarget.dataset.item;
      console.log(item);

    },
    clickAt(e){
      //点击 at 的用户名字
      var item = e.currentTarget.dataset.item;
      console.log(item);
    }
  }
})
